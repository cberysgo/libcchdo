"""Standard filenames used in the CCHDO data directory.

"""
EXPOCODE_FILENAME = 'ExpoCode'
README_FILENAME = '00_README.txt'
README_TEMPLATE_FILENAME = 'template.' + README_FILENAME
README_FINALIZED_FILENAME = '00_README.finalized.txt'
PROCESSING_EMAIL_FILENAME = 'processing_email.txt'
UOW_CFG_FILENAME = 'uow.json'
CRUISE_META_FILENAME = 'cruise.json'
FILE_MANIFEST_FILENAME = 'file_manifest.txt'
